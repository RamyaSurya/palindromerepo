﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PalidromeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var result = Palindrome();
            Console.WriteLine("OutPut: " + result);
            Console.ReadKey();

        }

        public static bool Palindrome() {
            Console.WriteLine("Enter Input: ");
            int input = Convert.ToInt32(Console.ReadLine());
            var num = input.ToString();
            var length = input.ToString().Length;
            var result = false;
            for (int k = 0; k < length-1; k++) {
                if (num[k] == num[k+1]) {
                    result = true;
                    return result;
                }
            }
            return result;
           
        }
    }
}
